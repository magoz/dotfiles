# -------------------------------------- 
# ---------  INSTALL PACKAGES ----------
# -------------------------------------- 

echo "🍺 Installing packages with Homebrew"
brew install \
  bat \
  neovim \
  stow \
  zsh \

echo "🔌 Installing vim-plug"
sh -c 'curl -fLo $HOME/.config/nvim/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

