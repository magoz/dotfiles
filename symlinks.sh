#!/bin/bash

# -------------- zsh -------------- 
ln -sf ~/dev/dotfiles/.zshrc ~/.zshrc

# -------------- oh my zsh -------------- 
ln -sf ~/dev/dotfiles/.oh-my-zsh ~/.oh-my-zsh

# -------------- Neovim -------------- 
# neovim dependencies
npm install -g typescript typescript-language-server

# neovim config 
mkdir -p ~/.config/nvim
mkdir -p ~/.config/nvim/plugin
ln -sf ~/dev/dotfiles/nvim/init.vim ~/.config/nvim/init.vim
ln -sf ~/dev/dotfiles/nvim/plugin/* ~/.config/nvim/plugin/

# -------------- Tmux -------------- 
ln -sf ~/dev/dotfiles/.tmux.conf ~/.tmux.conf
