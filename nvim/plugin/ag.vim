" ++++++++++++++ Ag ++++++++++++++++
" Make ack.vim plugin work with Ag
let g:ackprg = "ag --vimgrep"
let g:ack_default_options = " --case-sensitive --noheading --nopager --nocolor --nogroup --column"
