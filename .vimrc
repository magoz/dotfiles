" Auto-install vim-plug if not installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif



" ------------------------------
"           PLUGINS
" ------------------------------

call plug#begin('~/.vim/plugged')


" +++++++++++ THEMES +++++++++++

" Eldar
Plug 'agude/vim-eldar'

" Pencil
Plug 'reedes/vim-colors-pencil'


" +++++++++++ GENERAL +++++++++++

" ack / ag search
Plug 'mileszs/ack.vim'

" airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" sidebar files
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

" auto-pairs
Plug 'jiangmiao/auto-pairs'

" sorround
Plug 'tpope/vim-surround'

" ++++++ WRITING / MARKDOWN +++++++

" distraction Free
Plug 'junegunn/goyo.vim' 

" markdown
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & npm install'  }

" Wordy
Plug 'reedes/vim-wordy'

call plug#end()


" ------------------------------
"  PLUGINS CONFIG
" ------------------------------

" +++++++ Eldar Theme ++++++++++
if has('syntax')
  " Override Eldar GUI colors
    let g:eldar_red_bright     = "#ff0000"
    let g:eldar_yellow  = "#ffff00"
    let g:eldar_green   = "#00ff00"
    let g:eldar_cyan    = "#00ffff"
    let g:eldar_blue    = "#0000ff"
    let g:eldar_magenta = "#ff00ff"
    
    syntax enable             " Turn on syntax highlighting
    silent! colorscheme eldar " Custom color scheme
endif

" ++++++++ pencil Theme ++++++++
let g:pencil_terminal_italics = 1
" let g:pencil_spell_undercurl = 1
" colorscheme pencil
" set background=light

" +++++++++ airline themes ++++++
let g:airline_theme='simple'
" Buffer Separators
" let g:airline#extensions#tabline#enabled = 1
" let g:airline#extensions#tabline#left_sep = ' '
" let g:airline#extensions#tabline#left_alt_sep = '|'

" ++++++++++++++ Ag ++++++++++++++++
" Make ack.vim plugin work with Ag
let g:ackprg = "ag --vimgrep"
let g:ack_default_options = " --case-sensitive --noheading --nopager --nocolor --nogroup --column"

" ++++++++++++++ NerdTree ++++++++++++++++
" Show Hidden Files
let NERDTreeShowHidden=1
" Quit NerdTree on Open
let NERDTreeQuitOnOpen = 1
" Quit NerdTree if NerdTree is the last remaining window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Remove unnecessary UI
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

" ++++++++ markdown +++++++
" let g:mkdp_refresh_slow=1


" ++++++++ Goyo +++++++++++
" display line numbers
" let g:goyo_linenr = 1

" Ensure :q to quit even when Goyo is active
" https://github.com/junegunn/goyo.vim/wiki/Customization#ensure-q-to-quit-even-when-goyo-is-active
function! s:goyo_enter()
  let b:quitting = 0
  let b:quitting_bang = 0
  autocmd QuitPre <buffer> let b:quitting = 1
  cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction

function! s:goyo_leave()
  " Quit Vim if this is the only remaining buffer
  if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
    if b:quitting_bang
      qa!
    else
      qa
    endif
  endif
endfunction

autocmd! User GoyoEnter call <SID>goyo_enter()
autocmd! User GoyoLeave call <SID>goyo_leave()


" ------------------------------
"         STYLE SETTINGS
" ------------------------------

" Use italics for comments
highlight Comment cterm=italic

" Use pencil theme for markdown files
" autocmd BufEnter * colorscheme default
" autocmd BufEnter *.md colorscheme pencil



" ------------------------------
"         BASIC SETTINGS 
" ------------------------------

" Relative Numbers
set number relativenumber

" Mouse support
" set mouse=a

" Search Highlight as you type
set incsearch

" Remove Seach highlights after hitting enter
nnoremap <silent> <cr> :noh<CR><CR>

"" Search - case insensitive except when uppercase characters
set ignorecase
set smartcase

"Search - highlight current search
set hlsearch

" Indentation
set smarttab
set smartindent
set autoindent
set expandtab
set shiftwidth=2
set tabstop=2

" Treat words separated by dash as independant words
set iskeyword+=-

" Prevent adding new comment on next line when hitting enter
" https://vim.fandom.com/wiki/Disable_automatic_comment_insertion
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Spelling
set spellfile=.vim/spell/en.utf-8.add

" Disable spelling for code mode
:set nospell


" Change cursor shape between insert and normal mode in tmux and iTerm
" https://gist.github.com/andyfowler/1195581
if exists('$TMUX')
  let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
  let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

" Turn indentation off when pasting code
" https://coderwall.com/p/if9mda/automatically-set-paste-mode-in-vim-when-pasting-in-insert-mode
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

function! XTermPasteBegin()
  set pastetoggle=<Esc>[201~
  set paste
  return ""
endfunction

" -------------------------------
"           KEY REMAPS
" -------------------------------

" remap escape to jj
inoremap kk <Esc>
inoremap jj <Esc>
inoremap jk <Esc>

" Navigate up and down naturally (no weird line jump)
nmap j gj
nmap k gk

" MOVE LINES
" https://dockyard.com/blog/2013/09/26/vim-moving-lines-aint-hard
" Move lines in Normal mode
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==

" Move lines in Insert mode
inoremap <C-j> <ESC>:m .+1<CR>==gi
inoremap <C-k> <ESC>:m .-2<CR>==gi

" Move lines in Visual mode
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" DUPLICATE LINES
" https://www.quora.com/How-can-I-copy-the-previous-line-in-Vim-What-is-the-command-for-that
" Vim cannot map multiple modifiers at the same time
" https://stackoverflow.com/questions/1506764/how-to-map-ctrla-and-ctrlshifta-differently
" For this reason we are using <C-h> and <C-l> instead of the ideal <C-S-j> and <C-S-k>
" Duplicate lines in Normal mode
nnoremap <C-h> :t.<CR>==
nnoremap <C-l> :t-1<CR>==

" Duplicate lines in Insert mode
inoremap <C-h> <ESC> :t.<CR>==gi
inoremap <C-l> <ESC> :t-1<CR>==gi

" Duplicate lines in Visual mode
vnoremap <C-h> :t '>.<CR>==
vnoremap <C-l> :t '<-1<CR>==

" Make double-<Esc> clear search highlights
" https://stackoverflow.com/a/19877212
nnoremap <silent> <Esc><Esc> <Esc>:nohlsearch<CR><Esc>

" Open/Close Nerdtree
nnoremap <Leader>t :NERDTreeToggle<Enter>


" -------------------------------
"           MODES 
" -------------------------------

" Set Writing environment for .md files
augroup VimWriteMode
    au!
    autocmd FileType markdown colorscheme pencil
    autocmd FileType markdown set background=light
    autocmd FileType markdown setlocal spell spelllang=en_us
    autocmd FileType markdown set linebreak
    autocmd FileType markdown Goyo 70 
augroup END
